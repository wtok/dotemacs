;; Style ---------------------------------------------------------------
;; points of personal preference, excluding any hooks (see hooks.el)

;; Column limiting
;; (72 fits two panes horizontally on my chromebook)
;;(setq-default column-enforce-n 72)
;;(setq-default column-enforce-mode t)

;; Clean indent in org-mode
(setq-default org-startup-indented t)

;; Disable splash
(setq inhibit-splash-screen t)

;; Enable upcase-region
(put 'upcase-region 'disabled nil)

;; Enable downcase-region
(put 'downcase-region 'disabled nil)

;; use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;; Line numbers
;;(setq linum-format "%d  ")
;;(global-linum-mode t)
;; also consider nlinum-mode for speed

;; Chicken stuff
(setq scheme-program-name "csi -:c")

;; (setq whitespace-display-mappings  
;;       ((space-mark 32 [183 46])
;;        (space-mark 160 [164 95])
;;        (newline-mark 10 [2192 10]) ;; 170: ¬
;;        (tab-mark 9 [187 9] [92 9]))) ;; 

;; 2192 →

